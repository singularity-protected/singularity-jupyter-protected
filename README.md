# singularity-jupyter-protected

Jupyterlab notebook server in Singularity with Bitfusion Flexdirect, tensorflow, and Pytorch
suitable for running in the protected data network/PACE.


## how it works

We assume that the singularity image is here
`````
     /srv/singularity-images
`````

and that there are a couple bash shell scripts and a jupyter config file template  
that are also in that directory:
`````
     jupyter_notebook_config.py
     findport-jupyterlab
     run-jupyterlab
````` 
The run-jupyterlab script copies the jupyter config file template into the user  
home directory tree and also copies the findport-jupyterlab script into the user's homedir.  
Singularity overlays the host's unix filesystem except (by default) for the user homedir  
and /tmp, and we assume we are running from the user's homedir.

After putting the files in place, run-jupyterlab tells singularity to exec the flexdirect  
command to connect to a vGPU in the Bitfusion Flexdirect cluster and then run findport-jupyterlab.

Findport-jupyterlab locates an unused port so there will not be port collision for web access,  
tells the user how to connect via ssh port forwarding to the singularity container, generates a  
token to be used to authenticate to jupyterlab and launches jupyterlab within the container.  

## how it builds

Because we need the Bitfusion Flexdirect application and libraries to connect to the vGPUs we  
need to have flexdirect installed -- but this is not easily automated. To dance around that issue  
we build the Singularity container from a base image that is a Docker container that had flexdirect  
installed before beging converted to be a singularity container. This is the same base container we  
use for the Docker images for ECE590.

Once you have a Docker image with flexdirect installed (see below), you can run a local docker  
repository and tell Singularity to fetch the base image from the local repository. Ideally this would  
be in the Singularity docs, but until that happy day, here is a clue


## clue

originally found here: https://groups.google.com/a/lbl.gov/forum/#!searchin/singularity/docker$20local%7Csort:date/singularity/1OzBUM6uEow/d8om_vFeAwAJ

     This pattern is also one that you can follow:
     
     # Start a docker registry
     $ docker run -d -p 5000:5000 --restart=always --name registry registry:2
     
     # Push local docker container to it
     $ docker tag alpine localhost:5000/alpine
     $ docker push localhost:5000/alpine
     
     # Create def file for singularity like this..
     # (add your modifications)
     Bootstrap: docker
     Registry: http://localhost:5000
     Namespace:
     From: alpine:latest
     
     # Build singularity container
     $ sudo SINGULARITY_NOHTTPS=1 singularity build alpine.simg def



## Docker base image for Bitfusion Flexdirect

Flexdirect needs to run on an Ubumntu 16.04 base, and has an installer that is currently assumed to be
run interactively. Thus, we need to build a base image with appropriate libraries which we can then use 
as the base for the automated JupyterLab build. 

The Bitfusion base image source is found in the  bitfusion-docker-base-image subdirectory. After you build
that docker image, there are a couple manual steps to install Flexdirect and create a new Docker image

 1.) Run a bash shell inside a container running the image we build here
````
       sudo docker run --name bf-install -it ubuntu-bitfusion
````
 2.) From the bash shell, follow the Bitfusion directions to install 
 the flexdirect client

 3.) Exit the shell. There will be a dead container - find it with the command:
````
        sudo docker ps -a
````
 then commit the changes found in the dead container with this command:
 ````
        sudo docker commit -m="Ubuntu 16.04 with Flexdirect client" 5da4d4af1a72
````
 4.) Find the new untagged image with the command 
````
       sudo docker images
````
then tag the image using this command:
````
       sudo docker tag 1ac2779f1c66 ubuntu16.04-flexdirect
````







